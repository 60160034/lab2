module.exports = {
    isUserNameValid: function(username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        //Piano -> piano 
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function(age) {
        var num = parseInt(age);
        if (!(age.match(/^-{0,1}\d+$/))) {
            return false;
        }
        if (age < 18 || age > 100) {
            return false;
        }
        return true;
    },
    isPasswordValid: function(pass) {
        var i;
        var intpass = 0;
        for (i = 0; i < pass.length; i++) {
            var a = pass.charAt(i);
            if ((a.match(/^-{0,1}\d+$/))) {
                intpass += 1;
            }
        }
        if (pass.length < 7 || intpass < 2) {
            return false;
        }
        if (pass.toLowerCase() == pass) {
            return false;
        }
        if (!(pass.match(/[\W]/))) {
            return false;
        }
        return true;
    },
    isDateValid: function(day, month, year) {
        var sum = (year % 400);
        var sum2 = (year % 100);
        if (day > 31 || day < 1 || month > 12 || month < 1 || year > 2020 || year < 1970) {
            return false
        }
        if (((month == 4 || month == 6 || month == 9 || month == 11) && !(day < 31))) {
            return false
        }
        if ((sum !== 0) || (sum2 !== 0)) {
            if ((month == 2) && !(day < 29)) {
                return false;
            }
        }

        return true;

    }
}