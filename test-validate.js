const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', () => {
    context('function isUserNameValid', () => {
        it('Function prototype : boolean isUserNameValid(username: String)', () => {
            expect(validate.isUserNameValid('piano')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', () => {
            expect(validate.isUserNameValid('ok')).to.be.false;
        });
        it('ทุกตัวต้องเป็นตัวเล็ก', () => {
            expect(validate.isUserNameValid('Piano')).to.be.false;
            expect(validate.isUserNameValid('piaNo')).to.be.false;
        });
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', () => {
            expect(validate.isUserNameValid('ppp123456789012')).to.be.true;
            expect(validate.isUserNameValid('piano123456789012')).to.be.false;
        });
    });
    context('function isAgeValid ', () => {
        it('Function prototype : boolean isAgeValid (age: String)', () => {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข', () => {
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', () => {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('101')).to.be.false;
            expect(validate.isAgeValid('100')).to.be.true;

        });
    });

    context('function isPasswordValid ', () => {
        it('Function prototype : boolean isUserNameValid(password: String)', () => {
            expect(validate.isPasswordValid('P+456789')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', () => {
            expect(validate.isPasswordValid('P4567')).to.be.false;
            expect(validate.isPasswordValid('P+23456789')).to.be.true;
        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('ppiano+23456789')).to.be.false;
            expect(validate.isPasswordValid('Ppi+23456789')).to.be.true;
        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', () => {
            expect(validate.isPasswordValid('123+Acdefg')).to.be.true;
            expect(validate.isPasswordValid('12Abcdefg')).to.be.false;
        });
        it('ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=\`{}[]:";<>?,./ อย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('123Adefgf')).to.be.false;
            expect(validate.isPasswordValid('123+Acdefg')).to.be.true;
        });
    });

    context('function isDateValid', () => {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer', () => {
            expect(validate.isDateValid('12', '04', '1998')).to.be.true;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid('32', '04', '1998')).to.be.false;
            expect(validate.isDateValid('0', '04', '1998')).to.be.false;
            expect(validate.isDateValid('1', '04', '1998')).to.be.true;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid('2', '14', '1998')).to.be.false;
            expect(validate.isDateValid('2', '-1', '1998')).to.be.false;
            expect(validate.isDateValid('2', '10', '1998')).to.be.true;
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', () => {
            expect(validate.isDateValid('2', '07', '2030')).to.be.false;
            expect(validate.isDateValid('2', '07', '1900')).to.be.false;
            expect(validate.isDateValid('2', '07', '2019')).to.be.true;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกันตามรายการดังต่อไปนี้', () => {
            expect(validate.isDateValid('31', '04', '2019')).to.be.false;
            expect(validate.isDateValid('31', '02', '2019')).to.be.false;
            expect(validate.isDateValid('28 ', '02', '2018')).to.be.true;
            expect(validate.isDateValid('31', '01', '2019')).to.be.true;

        });
        if ('ในกรณีของปี อธิกสุรทิน ให้คำนวนตามหลักเกณฑ์นี้ ', () => {
                expect(validate.isDateValid('29', '02', '1970')).to.be.false;
                expect(validate.isDateValid('29', '02', '1990')).to.be.false;
                expect(validate.isDateValid('25', '02', '1970')).to.be.true;
                expect(validate.isDateValid('29', '02', '2000')).to.be.true;

            });

    });

});